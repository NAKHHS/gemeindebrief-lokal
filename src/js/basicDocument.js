/* eslint-disable prefer-const */
import * as lmodPages from './pages'
import * as lmodElements from './elements'
import * as lmodHelper from './helper'
import GclassPL from './parishletter'

export default {
  Year: null,
  Month: null,
  District: null,
  Congregation: null,
  lobjPL: null,
  new (Year, Month, District, Congregation) {
    this.Year = Year
    this.Month = Month
    this.District = District
    this.Congregation = Congregation
    this.lobjPL = new GclassPL(
      this.Year,
      this.Month,
      this.District,
      this.Folder,
      false,
      false
    )
    this.add(lmodPages.default.getDefaultPage(0))
  },
  add (DataToAdd) {
    this.XMLDoc.children[0].children[1].children.push(DataToAdd)
  },
  createNewEdition (
    DateValues,
    Congregation,
    Configuration,
    Impress,
    RecurringEvents,
    Pastors,
    Cite,
    OutputPath,
    District
  ) {
    // Insert and build front page
    lobjDocument.children[0].children[1].children.push(
      lmodPages.default.getDefaultPage(0)
    )
    const lobjFrontPageObjects = lmodPages.default.FrontPageElements(
      DateValues,
      Congregation,
      Configuration,
      Cite
    )
    Object.keys(lobjFrontPageObjects).forEach(lstrKey => {
      lobjDocument.children[0].children[1].children.push(
        lobjFrontPageObjects[lstrKey]
      )
    })
    // Insert an build impress
    lobjDocument.children[0].children[1].children.push(
      lmodPages.default.getDefaultPage(1)
    )
    lobjDocument.children[0].children[1].children.push(
      lmodElements.default.PageCategory(1, [
        'REGELMÄSSIGE VERANSTALTUNGEN',
        'IMPRESSUM'
      ])
    )
    lobjDocument.children[0].children[1].children.push(
      lmodElements.default.impressHead(1, Congregation, Impress)
    )
    const lobjImpressRecurringEvents = lmodElements.default.impressRecurringEvents(
      1,
      RecurringEvents
    )
    lobjDocument.children[0].children[1].children.push(
      lobjImpressRecurringEvents.objects
    )
    const lobjPastors = lmodElements.default.Pastors(
      1,
      Pastors,
      lobjImpressRecurringEvents.Y
    )
    lobjDocument.children[0].children[1].children.push(lobjPastors.objects)
    lobjPastors.Y += lmodHelper.default.impressRowToMM(1) / 2
    lobjDocument.children[0].children[1].children.push(
      lmodElements.default.fullWidthLine(1, lobjPastors.Y)
    )
    lobjPastors.Y += lmodHelper.default.impressRowToMM(1) / 2
    lobjDocument.children[0].children[1].children.push(
      lmodElements.default.ImpressImpress(1, lobjPastors.Y, Impress, District)
    )
    /* this.writeXML(
      this.gobjXML,
      lmodPath.join(
        OutputPath,
        Congregation.folder,
        'Ausgabe',
        'GB.sla'
      )
    ) */
  }
}
