import * as lmodHelper from './helper'

export default {
  fpHead () {},
  fpLine () {},
  fpMonthYear () {},
  fpNACCharitable () {},
  fpCite () {},
  fpLogo () {},
  fpDistChurchName () {},
  fullWidth: 362.834645669291, // 128mm
  fullWidthBlock (Height, X, Y, PageNumber, Style) {
    return this.Block(Height, 128, X, Y, PageNumber, Style)
  },
  fullWidthLine (PageNumber, Y) {
    return this.Line(PageNumber, 10, Y, 128)
  },
  Line (PageNumber, X, Y, Width) {
    const lobjLine = lmodHelper.default.loadElement('defHorizontalLine')
    lobjLine.attributes.XPOS = X
    lobjLine.attributes.YPOS = Y
    lobjLine.attributes.WIDTH = Width
    lobjLine.attributes.OwnPage = PageNumber
    return lobjLine
  },
  Block (Height, Width, X, Y, Page, PStyle) {
    const lobjBlock = lmodHelper.default.loadElement('defBlock')
    lobjBlock.attributes.XPOS = lmodHelper.default.mmToScribusUnit(X)
    lobjBlock.attributes.YPOS = lmodHelper.default.mmToScribusUnit(Y)
    lobjBlock.attributes.WIDTH = lmodHelper.default.mmToScribusUnit(Width)
    lobjBlock.attributes.HEIGHT = lmodHelper.default.mmToScribusUnit(Height)
    lobjBlock.attributes.OwnPage = Page
    lobjBlock.attributes.PSTYLE = PStyle
    return lobjBlock
  },
  TextBlock (PageNumber, Height, Width, X, Y, DefaultStyle, Text) {
    const lobjBlock = this.Block(
      Height,
      Width,
      X,
      Y,
      PageNumber,
      DefaultStyle
    )
    lobjBlock.children = []
    lobjBlock.children.push(this.createTextContent(DefaultStyle, Text))
    return lobjBlock
  },
  Pastors (PageNumber, Pastors, Y) {
    let lintY = Y + (lmodHelper.default.impressRowToMM(1) / 2)
    const lobjElements = []
    lobjElements.push(
      this.fullWidthBlock(lmodHelper.default.impressRowToMM(1), 10, lintY, PageNumber, 'Impressum - Anschriften-Normal')
    )
    Pastors.forEach(lobjPastor => {
      if (lobjPastor.special !== '') {
        lobjElements.push(this.TextBlock(PageNumber, lmodHelper.default.impressRowToMM(lobjPastor.rows), 56, 10, lintY, 'Termine -links', lobjPastor.special))
      }
      const lstrText = lmodHelper.default.buildPastorAddress(lobjPastor)
      lobjElements.push(
        this.TextBlock(
          PageNumber,
          lmodHelper.default.impressRowToMM(lobjPastor.rows),
          72,
          66,
          lintY, 'Impressum - Anschriften-Normal',
          lstrText
        )
      )

      lintY += lmodHelper.default.impressRowToMM(lobjPastor.rows)
    })
    return { objects: lobjElements, Y: lintY }
  },
  ImpressImpress (PageNumber, Y, Impress, District) {
    const lobjImpressConfig = lmodHelper.default.loadConfig(District + '_settings')
    const lobjElements = []
    lobjImpressConfig.Impress.forEach(lobjImpressEntry => {
      // const lstrType = lobjImpressEntry.type
      const lintHeight = lmodHelper.default.impressRowToMM(
        lobjImpressEntry.rows
      )
      if (Array.isArray(lobjImpressEntry.data)) {
        lobjImpressEntry.data.forEach(lobjDataEntry => {
          lobjElements.push(this.TextBlock(
            PageNumber, lintHeight, 56, 10, Y, 'MissingStyle', 'Text'
          ))
        })
      } else {
        if (lobjImpressEntry.type === 'constant') {
        }
        lobjElements.push(
          this.TextBlock(
            PageNumber,
            lmodHelper.default.impressRowToMM(lobjImpressEntry.rows)
          )
        )
      }
    })
    // V.i.S.d.P. Niedersächsisches Pressegesetz:
  },
  impressRecurringEvents (PageNumber, RecurringEvents) {
    let lintY = 37
    const lobjElements = []
    Object.keys(RecurringEvents).forEach(lstrEvent => {
      lobjElements.push(
        this.recurringEvent(
          PageNumber,
          lmodHelper.default.impressRowToMM(RecurringEvents[lstrEvent].rows),
          lintY,
          lstrEvent,
          RecurringEvents[lstrEvent]
        )
      )
      lintY += lmodHelper.default.impressRowToMM(
        RecurringEvents[lstrEvent].rows
      )
    })
    lobjElements.push(
      this.TextBlock(
        PageNumber,
        lmodHelper.default.impressRowToMM(1),
        72,
        66,
        lintY,
        'Termine - rechts',
        'Kurzfristige Terminänderungen vorbehalten'
      )
    )
    return { objects: lobjElements, Y: lintY }
  },
  recurringEvent (PageNumber, Height, Y, Eventname, Eventtext) {
    const lobjElements = []
    lobjElements.push(
      this.TextBlock(
        PageNumber,
        Height,
        56,
        10,
        Y,
        'Termine - links',
        Eventname
      )
    )
    lobjElements.push(
      this.TextBlock(
        PageNumber,
        Height,
        72,
        66,
        Y,
        'Termine - rechts',
        Eventtext
      )
    )
    return lobjElements
  },
  impressHead (PageNumber, Congregation, Impress) {
    const lobjBlock = this.Block(
      12,
      128,
      10,
      20,
      PageNumber,
      'Impressum - Gemeindeanschrift'
    )
    lobjBlock.children = []
    lobjBlock.children.push(
      this.createTextContent('Impressum - Gemeindeanschrift', [
        {
          text:
            'Gemeindebrief der Neuapostolischen Kirche Nord- und Ostdeutschland',
          format: 'Impressum - Gemeindeanschrift'
        },
        {
          text: 'Gemeinde ' + Congregation.Name + ' - Herausgeber -',
          format: 'Impressum - Gemeindeanschrift'
        },
        {
          text:
            Impress.zip +
            ' ' +
            Impress.city +
            ', ' +
            Impress.street +
            ' ' +
            Impress.number +
            ' ' +
            (Impress.internet !== '' ? '| Internet:' + Impress.internet : ''),
          format: 'Impressum - Gemeindeanschrift'
        }
      ])
    )
    return lobjBlock
  },
  createTextContent (DefaultStyle, Content) {
    // eslint-disable-next-line prefer-const
    let lobjText = []
    lobjText.push(this.DefaultStyle(DefaultStyle))
    for (let lintCounter = 0; lintCounter < Content.length; lintCounter++) {
      lobjText.push(this.IText(Content[lintCounter].content))
      if (lintCounter === Content.length - 1) {
        lobjText.push(this.Trail(Content[lintCounter].format))
      } else {
        lobjText.push(this.Para(Content[lintCounter].format))
        lobjText.push(this.Linebreak())
      }
    }

    /* if (typeof (Content) === 'string') {
      lobjText.push(this.IText(Content))
      lobjText.push(this.Trail(DefaultStyle))
    } else if (typeof (Content) === 'object') {
      let lstrText = ''
      if (Object.prototype.hasOwnProperty.call(Content, 'connector')) {
        const lstrConnector = Content.connector
        Content.data.forEach(lmiscEntry => {
          if (typeof (lmiscEntry) === 'object') {
            let lstrLocalConnector = ''
            if (Object.prototype.hasOwnProperty.call(lmiscEntry, 'connector')) {
              lstrLocalConnector = lmiscEntry.connector
              delete lmiscEntry.connector
            }
            Object.keys(lmiscEntry).forEach(lstrKey => {
              lstrText += lmiscEntry[lstrKey] + lstrLocalConnector
            })
            lstrText = lstrText.substring(0, lstrText.length - lstrLocalConnector.length)
          } else {
            lstrText += lmiscEntry
          }
          lstrText += lstrConnector
        })
        lstrText = lstrText.substring(0, lstrText.length - lstrConnector.length)
        lobjText.push(this.IText(lstrText))
        lobjText.push(this.Trail(DefaultStyle))
      } else if (Array.isArray(Content)) {
        for (let lintCounter = 0; lintCounter < Content.length; lintCounter++) {
          if (lintCounter === Content.length - 1) {
            lobjText.push(this.IText(Content[lintCounter]))
            lobjText.push(this.Trail(DefaultStyle))
          } else {
            lobjText.push(this.IText(Content[lintCounter]))
            lobjText.push(this.Para(DefaultStyle))
            lobjText.push(this.Linebreak())
          }
        }
      } else {
        lobjText.push(this.IText(Content.data))
        lobjText.push(this.Trail(DefaultStyle))
      }
    } else {
      for (let lintCounter = 0; lintCounter < Content.length; lintCounter++) {
        lobjText.push(this.IText(Content[lintCounter].text))
        if (lintCounter === Content.length - 1) {
          lobjText.push(this.Trail(Content[lintCounter].format))
        } else {
          lobjText.push(this.Para(Content[lintCounter].format))
        }
      }
    } */

    return { type: 'element', name: 'StoryText', children: lobjText }
  },
  DefaultStyle (Format) {
    return {
      type: 'element',
      name: 'DefaultStyle',
      attributes: { PARENT: Format }
    }
  },
  IText (Text, FontSize = 0) {
    if (FontSize === 0) {
      return { type: 'element', name: 'ITEXT', attributes: { CH: Text } }
    } else {
      return {
        type: 'element',
        name: 'ITEXT',
        attributes: { CH: Text, FONTSIZE: FontSize }
      }
    }
  },
  Para (Format) {
    return { type: 'element', name: 'para', attributes: { PARENT: Format } }
  },
  Linebreak () {
    return { type: 'element', name: 'breakline' }
  },
  Trail (Format) {
    return { type: 'element', name: 'trail', attributes: { PARENT: Format } }
  },
  StoryText () {
    return { type: 'element', name: 'StoryText' }
  },
  CreateGroup (PageNumber, Width, Height, X, Y, Name) {
    let lobjGroup
    lobjGroup.type = 'element'
    lobjGroup.name = 'PAGEOBJECT'
    lobjGroup.attributes = lmodHelper.default.loadParams('group')
    // lobjGroup.attributes.
    // lmodHelper.default.loadParams('group')
  },
  AddToGroup (Group, Content) {

  },
  CreateLine () {
    /* {
      "type": "element",
      "name": "PAGEOBJECT",
      "attributes": {
        "XPOS": "128.346456692913",
        "YPOS": "4495.2755905512",
        "OwnPage": "7",
        "ItemID": "161943568",
        "
        ": "5",
        "WIDTH": "28.3464566929134",
        "HEIGHT": "1",
        "FRTYPE": "0",
        "CLIPEDIT": "0",
        "ROT": "90",
        "PWIDTH": "2.1259842519685",
        "PCOLOR": "NAK-blau 100%",
        "PCOLOR2": "NAK-blau 100%",
        "PLINEART": "1",
        "path": "",
        "gXpos": "1.06299212598425",
        "gYpos": "0",
        "gWidth": "363.897637795275",
        "gHeight": "28.3464566929133",
        "LAYER": "0"
      }
    } */
  },
  PageCategory (PageNumber, Text) {
    // Create Group
    // const lmodGroup = this.CreateGroup(PageNumber, 'Full', lmodHelper.default.impressRowToMM(2))
    // Add to group -> Text
    // Add to group -> Line

    // return lobjPageCategory
  },
  // ---------------------------- Params
  getGroupParams () {

  }
}
