/* eslint-disable prefer-const */
const lmodFS = require('fs')
const lmodPath = require('path')
const parseXml = require('@rgrove/parse-xml')
const lmodHelp = require('./helper')
const lmodPages = require('./pages')
// const conversionFactor = 2.834645669

export default class PL {
  // 1mm = 2.834645669
  conversionFactor = 2.834645669;
  // 100% = 0.24
  scaleFactor = 0.24;
  PL = null
  Year = null
  Month = null
  District = null
  Congregation = null
  BW = null
  NummPages = 0
  Path = null

  constructor (Year, Month, District, Congregation, BW = false, Open = false) {
    this.Year = Year
    this.Month = Month
    this.District = District
    this.Congregation = Congregation
    this.BW = BW

    this.NumPages = 0
    this.buildPath()

    if (Open) {
      this.open()
    }
  }

  buildPath () {
    this.Path = lmodFS.readFileSync(
      lmodHelp.getPLPath(
        this.Year,
        this.Month,
        this.District,
        this.Congregation,
        this.BW
      )
    )
  }

  addPage () {
    this.addData(lmodPages.default.getDefaultPage(this.NumPages++))
  }

  addData (Data) {
    this.PL.children[0].children[1].children.push(Data)
  }

  addText (PageNum, Text) {}

  addContent (PageNum, Content) {}

  addCategory (PageNum, CategoryText) {}

  PageFooter () {}

  addSchedule (Page, File) {}

  addFrontPage (District, Congregation, Year, Month, Cite, CiteLocation) {}

  addImpress () {}

  open () {
    // this.PL = parseXml(lmodFS.readFileSync(lmodPath.join(process.cwd(), 'data', 'templates', 'empty.sla'), 'utf-8'))
    this.PL = parseXml(lmodFS.readFileSync(this.Path), 'utf-8')
    this.PL.children[0].children[1].children.forEach(lobjChild => {
      if (lobjChild.name === 'PAGE') {
        this.NumPages++
      }
    })
  }

  createNewEdition () {
    this.PL = parseXml(
      lmodFS.readFileSync(
        lmodPath.join(process.cwd(), 'data', 'templates', 'empty.sla'),
        'utf-8'
      )
    )
  }

  save () {
    let lstrXML = ''
    if (Object.prototype.hasOwnProperty.call(this.PL, 'type')) {
      if (this.PL.type === 'document') {
        lstrXML += '<?xml version="1.0" encoding="UTF-8"?>'
      }
    }
    if (Object.prototype.hasOwnProperty.call(this.PL, 'children')) {
      lstrXML += this.buildXMLString(this.PL.children)
    }
    lmodFS.writeFile(this.Path, lstrXML, function (err, data) {
      if (err) console.log(err)
      console.log('successfully written our update xml to file')
    })
  }

  cleanUpText (TextString) {
    if (typeof TextString === 'string') {
      return TextString.replace('&', '&amp;')
    }
  }

  buildXMLString (XMLData) {
    let lstrXML = ''
    if (XMLData.length > 0) {
      XMLData.forEach(lobjElement => {
        if (Object.prototype.hasOwnProperty.call(lobjElement, 'type')) {
          if (lobjElement.type === 'element') {
            lstrXML += '<' + lobjElement.name
            if (
              Object.prototype.hasOwnProperty.call(lobjElement, 'attributes')
            ) {
              Object.keys(lobjElement.attributes).forEach(lstrKey => {
                lstrXML +=
                  ' ' +
                  lstrKey +
                  '="' +
                  this.cleanUpText(lobjElement.attributes[lstrKey]) +
                  '"'
              })
            }
            if (Object.prototype.hasOwnProperty.call(lobjElement, 'children')) {
              if (lobjElement.children.length > 0) {
                lstrXML += '>'
                lstrXML += this.buildXMLString(lobjElement.children)
                lstrXML += '</' + lobjElement.name + '>'
              } else {
                lstrXML += '/>'
              }
            } else {
              lstrXML += '/>'
            }
          }
        }
      })
    }
    return lstrXML
  }
}
