/* eslint-disable prefer-const */
const lmodFS = require('fs')
import PL from './parishletter'
const parseXml = require('@rgrove/parse-xml')
// const conversionFactor = 2.834645669
// eslint-disable-next-line no-unused-vars
let gobjXML = null

export default {
  createNewEdition (Year, Month, District, Congregation, Cite, CiteLocation) {
    const lobjPL = new PL(Year, Month, District, Congregation, false, false)
    lobjPL.addFrontPage(
      Year,
      Month,
      District,
      Congregation,
      Cite,
      CiteLocation
    )
  },
  loadDocument (DocumentPath, Return = false) {
    if (Return) {
      return parseXml(lmodFS.readFileSync(DocumentPath, 'utf-8'))
    } else {
      this.gobjXML = parseXml(lmodFS.readFileSync(DocumentPath, 'utf-8'))
    }
  },
  getNumberOfPages () {
    let lintCounter = 0
    this.gobjXML.children[0].children[1].children.forEach(lobjChild => {
      if (lobjChild.name === 'PAGE') {
        lintCounter++
      }
    })
    return lintCounter
  }

}
