/* eslint-disable prefer-const */
// const lmodElements = require('./elements')
const lmodFS = require('fs')
const lmodPath = require('path').join
import * as lmodHelp from './helper'
// import * as lmodElements from './elements'

export default {

  Impress (Data, PageNumber) {

  },
  SchedulePage (Data, isNewDocument = false) {
    if (isNewDocument) {
      JSON.parse(
        lmodFS.readFileSync(
          lmodPath(process.cwd(), 'data', 'elements', 'defPage.json')
        )
      ).attributes.NUM = 1
    } else {
      JSON.parse(
        lmodFS.readFileSync(
          lmodPath(process.cwd(), 'data', 'elements', 'defPage.json')
        )
      ).attributes.NUM = Data.PageNum
    }
  },
  FrontPageElements (DateValues, Congregation, Configuration, Cite) {
    const larrFrontPageData = [
      'fpBlueLine',
      'fpCite',
      'fpCongregationImage',
      'fpLogoKaritativ',
      'fpLogoNAC',
      'fpMonthYear',
      'fpNACText',
      'fpTitle'
    ]
    let lobjPageObjects = []
    larrFrontPageData.forEach(lstrFrontPageObj => {
      lobjPageObjects[[lstrFrontPageObj]] = JSON.parse(
        lmodFS.readFileSync(
          lmodPath(
            process.cwd(),
            'data',
            'elements',
            lstrFrontPageObj + '.json'
          )
        )
      )
    })

    // Cite
    // The cite element is the second element in the pageobjects
    lobjPageObjects.fpCite.children[0].children[1].attributes.CH = Cite.text
    lobjPageObjects.fpCite.children[0].children[3].attributes.CH = Cite.location
    // Congregation Image
    lobjPageObjects.fpCongregationImage.attributes.PFILE =
      'L:/GB/Bilder/Kirchen/Kirche_' + Congregation.name + '_HQ.tif'
    Object.keys(Configuration.frontPageImage).forEach(lstrKey => {
      lobjPageObjects.fpCongregationImage.attributes[lstrKey] =
          Configuration.frontPageImage[lstrKey]
    })

    // Logo Karitativ
    lobjPageObjects.fpLogoKaritativ.attributes.PFILE =
      'L:/GB/Bilder/Logos/Logo_NAK_karitativ_HQ.tiff'
    // Logo NAC
    lobjPageObjects.fpLogoNAC.attributes.PFILE =
      'L:/GB/Bilder/Logos/Logo_NAK_HQ.tif'
    // Month Year
    lobjPageObjects.fpMonthYear.children[0].children[1].attributes.CH =
      lmodHelp.default.intMonthToStrMonth(DateValues.Month) + ' ' + DateValues.Year
    // Title
    lobjPageObjects.fpTitle.children[0].children[4].attributes.CH =
      Congregation.name
    return lobjPageObjects
  },
  getDefaultPage (PageNumber) {
    let lobjDefaultPage = JSON.parse(
      lmodFS.readFileSync(
        lmodPath(process.cwd(), 'data', 'elements', 'defPage.json')
      )
    )
    lobjDefaultPage.attributes.NUM = PageNumber
    lobjDefaultPage.attributes.PAGEYPOS = (635.275590551179 * PageNumber) + 20

    return lobjDefaultPage
  }
}
