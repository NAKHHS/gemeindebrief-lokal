// const dayjs = require("dayjs");
const lmodFS = require('fs')
const lmodPath = require('path')
const lmodRFS = require('fs').readFileSync
const lmodWFS = require('fs').writeFileSync
const lmodPJ = require('path').join
const parseXml = require('@rgrove/parse-xml')
const conversionFactor = 2.834645669

export default {
  conversionFactor: 2.834645669,
  h1: 11.3385826771654, // 4mm
  h2: 19.8425196850394, // 7mm
  h3: 28.3464566929134, // 10mm
  h4: 39.6850393700787, // 14mm
  connectTexts (Texts) {

  },
  getCategoryShortcut (Category) {
    switch (Category) {
      case 'Textworte':
        return 'TW'
      case 'Wort zum Monat':
        return 'WzM'
    }
  },
  rowToMM (Rows) {
    return this.loadConfig('scribus').normalRows[Rows.toString()]
  },
  impressRowToMM (Rows) {
    return this.loadConfig('scribus').impressRows[Rows.toString()]
  },
  isEven (value) {
    if (value % 2 === 0) { return true } else { return false }
  },
  buildTextString (TextData) {
    let lstrText = ''
    if (Array.isArray(TextData)) {
      for (let lintCounter = 0; lintCounter < TextData.length; lintCounter++) {
        let lstrConnector = ', '
        if (lintCounter === lintCounter.length - 1) {
          lstrConnector = ' und '
        }
        if (Array.isArray(TextData[lintCounter])) {
          TextData[lintCounter].forEach(lstrString => {
            lstrText += lstrString + ' '
          })
          lstrText = lstrText.substring(0, lstrText.length - 1)
        } else if (typeof (TextData[lintCounter]) === 'object') {
          let lstrLocalConnector = ' '
          if (
            Object.prototype.hasOwnProperty.call(
              TextData[lintCounter],
              'connector'
            )
          ) {
            lstrLocalConnector = TextData[lintCounter].connector
            delete TextData[lintCounter].connector
          }
          Object.keys(TextData[lintCounter]).forEach(lstrKey => {
            lstrText += TextData[lintCounter][lstrKey] + lstrLocalConnector
          })
          lstrText = lstrText.substring(0, lstrText.length - lstrLocalConnector.length)
        }
        lstrText += lstrConnector
      }
      lstrText = lstrText.substring(0, lstrText.length - 5)
    }
    return lstrText
  },
  formatPhoneNumber (Phonenumber, Type, AreaCodeOrSubscriberNumber) {
    if (AreaCodeOrSubscriberNumber === 'AreaCode') {
    }
  },
  buildPastorAddress (Pastor) {
    const lobjPastor = []
    lobjPastor.push(Pastor.ministry + ' ' + Pastor.name[0] + ' ' + Pastor.name[1])
    if (Pastor.address.street !== '') {
      lobjPastor.push(Pastor.address.street + ' ' + Pastor.address.number + ', ' + Pastor.address.zip + ' ' + Pastor.address.city)
    }
    let lstrPhone = ''
    if (Pastor.mobile.length > 0) {
      lstrPhone += '(' + Pastor.mobile[0] + ') ' + Pastor.mobile[1]
    }
    if (Pastor.mobile.length > 0 && Pastor.phone.length > 0) {
      lstrPhone += ' | '
    }
    if (Pastor.phone.length > 0) {
      lstrPhone += '(' + Pastor.phone[0] + ') ' + Pastor.phone[1]
    }
    lobjPastor.push(lstrPhone)
    if (Pastor.email !== '') {
      lobjPastor.push(Pastor.email)
    }
    return lobjPastor
  },
  getHeightScribus (TypeOrMM) {
    switch (TypeOrMM) {
      case '4mm':
      case '1':
      case 'h1':
        return this.h1
      case '7mm':
      case '2':
      case 'h2':
        return this.h2
      case '10mm':
      case '3':
      case 'h3':
        return this.h3
      case '14mm':
      case '4':
      case 'h4':
        return this.h4
    }
  },
  getHeightMM (HeighElement) {
    switch (HeighElement) {
      case 'h1': return 4
      case 'h2': return 7
      case 'h3': return 10
      case 'h4': return 14
    }
  },
  checkCreateDirectory (PathToCheck) {
    if (!require('fs').existsSync(PathToCheck)) {
      require('fs').mkdirSync(PathToCheck)
    }
  },
  twoDigitsMonth (Month) {
    if (typeof Month === 'number') {
      Month = Month.toString()
    }
    if (Month.length === 1) {
      Month = '0' + Month
    }
    return Month
  },
  intMonthToStrMonth (IntMonth) {
    switch (IntMonth) {
      case 1:
        return 'Januar'
      case 2:
        return 'Februar'
      case 3:
        return 'März'
      case 4:
        return 'April'
      case 5:
        return 'Mai'
      case 6:
        return 'Juni'
      case 7:
        return 'Juli'
      case 8:
        return 'August'
      case 9:
        return 'September'
      case 10:
        return 'Oktober'
      case 11:
        return 'November'
      case 12:
        return 'Dezember'
    }
  },
  getPath (Value, Name = null, Name2 = null) {
    switch (Value.toLowerCase()) {
      case 'districtYears':
      case 'dy':
        break
      case 'b':
      case 'bible':
        return lmodPJ(process.cwd(), 'data', 'bible')
      case 'py':
      case 'python':
        return lmodPJ(process.cwd(), 'data', 'scripts', 'python')
      case 't':
      case 'template':
      case 'templates':
        return lmodPJ(process.cwd(), 'data', 'templates', Name + '.sla')
      case 'element':
      case 'elements':
      case 'e':
        return lmodPJ(process.cwd(), 'data', 'elements', Name + '.json')
      case 'params':
      case 'parameter':
      case 'p':
        return lmodPJ(process.cwd(), 'data', 'params', Name + '.json')
      case 'config':
      case 'configuration':
      case 'c':
        return lmodPJ(process.cwd(), 'data', 'settings', Name + '.json')
      case 'congregation':
      case 'con':
        return lmodPJ(
          process.cwd(),
          'data',
          'congregations',
          Name2,
          Name + '.json'
        )
    }
  },
  getPLPath (Year, Month, District, Congregation, BW = false) {
    let lstrBW = '_sw'
    if (!BW) { lstrBW = '' }
    return lmodPJ(process.cwd(), this.loadAppSetting().path, Year, Month, this.loadCongregationData(District, Congregation).folder, 'GB' + lstrBW + '.sla')
  },
  mmToScribusUnit (Millimeter) {
    return Millimeter * conversionFactor
  },
  ScribusUnitToMM (ScribusUnit) {
    return ScribusUnit / conversionFactor
  },
  loadElement (ElementName) {
    return this.loadJson(lmodPJ(process.cwd(), 'data', 'elements', ElementName + '.json'))
  },
  loadParams (ParamsName) {
    return this.loadJson(lmodPJ(process.cwd(), 'data', 'params', ParamsName + '.json'))
  },
  loadConfig (ConfigName) {
    return this.loadJson(lmodPJ(process.cwd(), 'data', 'settings', ConfigName + '.json'))
  },
  loadCongregationData (District, Congregation) {
    return this.loadJson(lmodPJ(process.cwd(), 'data', 'congregations', District, Congregation + '.json'))
  },
  loadAppSetting () {
    return this.loadConfig('settings')
  },
  loadYearsForDistrict (District, FS = true) {
    if (FS) {
      this.getFoldersInDir()
    }
  },
  loadCongregationsInDistrict (District) {
    return this.loadConfig(District)
  },
  loadContentSettings (District, Congregation, Year, Month, ContentName) {

  },
  loadTemplate (Template, Return = true) {
    const lstrPath = this.getPath('t', Template)
    if (Return) {
      return parseXml(lmodRFS(lstrPath, 'utf-8'))
    } else {
      this.gobjXML = parseXml(lmodRFS(lstrPath, 'utf-8'))
    }
  },
  loadDistricts () {
    return this.loadConfig('districts')
  },
  loadJson (Path) {
    return JSON.parse(lmodRFS(Path))
  },
  saveJson (Data, Path) {
    lmodWFS(Path, JSON.stringify(Data), 'utf-8')
  },
  getVersesCount (Book, Chapter) {
    const larrVerses = this.getVerses(Book, Chapter)
    return larrVerses.length
  },
  getVerses (Book, Chapter) {
    Book = Book.replace(' ', '')
    return this.loadJson(lmodPJ(this.getPath('b'), Book + '_' + Chapter + '.json'))
  },
  getVersesContent (Book, Chapter, From, To = null) {
    const larrVerses = this.getVerses(Book, Chapter)
    let lstrText = ''
    if (To === null) {
      lstrText = larrVerses[From - 1][From]
    } else {
      for (let lintCounter = From; lintCounter <= To; lintCounter++) {
        lstrText += lintCounter.toString() + ' ' + larrVerses[lintCounter - 1][lintCounter]
      }
    }
    return lstrText
  },
  getBibleBooks () {
    return this.loadConfig('bible')
  },
  getYearsForDistrict (District) {
    const lstrPath = this.loadConfig('settings').path
    return this.getFoldersInDir(lmodPath.join(lstrPath, District))
  },
  getMonthsForDistrictYear (District, Year) {
    const lstrPath = this.loadConfig('settings').path
    return this.getFoldersInDir(lmodPath.join(lstrPath, District, Year))
  },
  getCongregationsForDistrictYearMonth (District, Year, Month) {
    const lstrPath = this.loadConfig('settings').path
    return this.getFoldersInDir(lmodPath.join(lstrPath, District, Year, Month)).filter(lstrFolder => lstrFolder !== 'Ausgaben')
  },
  getFoldersInDir (Path) {
    let larrDirs = lmodFS.readdirSync(Path)
    larrDirs = larrDirs.filter(f => lmodFS.statSync(lmodPath.join(Path, f)).isDirectory())
    return larrDirs
  }

}
