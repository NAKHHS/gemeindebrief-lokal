export function addBreadCrumbs (State, Payload) {
  if (Array.isArray(Payload)) {
    Payload.forEach(lobjItem => {
      State.breadCrumbs.push(lobjItem)
    })
  } else {
    State.breadCrumbs.push(Payload)
  }
}
export function clearBreadCrumbs (State) {
  State.breadCrumbs = []
}
export function removeBreadCrumbs (State, Payload) {
  let lintRemove = -1
  for (let lintCounter = 0; lintCounter < State.breadCrumbs.length; lintCounter++) {
    if (State.breadCrumbs[lintCounter].name === Payload) {
      lintRemove = lintCounter
    }
  }

  State.breadCrumbs.splice(lintRemove, 1)
}
// ------------------------------------------------------------------------------------------
export function setModulesOptions (State, Payload) {
  Payload.forEach(lobjModule => {
    State.modules.push(lobjModule)
  })
}
export function setModule (State, Payload) {
  State.activeModule = Payload
}
export function onSection (State, Paylod) {
  State.fabs.sections.on = Paylod
}
export function clearReceivers (State) {
  /* Object.keys(State.receivers).forEach(lstrReceiver => {
    State.receivers[lstrReceiver] = false
  }) */
  State.receivers[State.activeComponent] = false
}
export function onAction (State, Payload) {
  State.receivers[State.activeComponent] = true
  State.fabs.actions.on = Payload
}
export function setActiveComponent (State, Payload) {
  State.activeComponent = Payload
}

export function resetOn (State, Payload) {
  if (Array.isArray(Payload)) {
    Payload.forEach(lstrFab => {
      State.fabs[lstrFab].on = null
    })
  } else {
    State.fabs[Payload].on = null
  }
}
export function setEditContent (State, Payload) {
  State.editContent.name = Payload
}

export function fakeHeader (State, Payload) {
  State.selectionHeader.district.value = Payload.district
  State.selectionHeader.congregation.value = Payload.congregation
  State.selectionHeader.year.value = Payload.year
  State.selectionHeader.month.value = Payload.month
}
